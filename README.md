# Exo Spring
Projet avec des petits exercices sur Spring. A la cool.

## Contrôleur-template
1. Créer un contrôleur HomeController, avec dedans une méthode showHomePage qui sera un get sur "/"
2. Créer un template index.html dans le resources/templates et dedans faire juste une page d'accueil avec un h1 et pis voilà. Puis faire en sorte d'afficher ce template via le contrôleur quand on va sur "/"
3. Dans le contrôleur, déclarer une variable message de type string, et la donner à manger au Model pour ensuite l'afficher dans le template via un th:text
4. Toujours dans la méthode showHomePage, faire en sorte de générer un nombre au hasard entre 1 et 10 par exemple et donner ce nombre au Model pour l'afficher également dans le template
5. Rajouter un petit th:if dans le template pour afficher "Greater than 5" seulement si le nombre est supérieur à 5 

### With Loop
1. Créer une nouvelle route en GET sur /loop
2. Dans cette route, créer une variable de type List<String> en lui mettant quelques string, peu importe quoi
3. Créer un template loop.html, et donner la liste du contrôleur au template
4. Dans le template, faire en sorte, via un th:each d'afficher chaque string de la liste


## TodoList
1. Créer un nouveau controller TodoController
2. Dans ce controller, créer une méthode en get pour l'affichage de la TodoList, avec son template sur la route "/todo"
3. Créer une propriété de type List<String> sur le TodoController et initialiser le vide ou avec 2-3 entrées dedans, peu importe (mais si vous l'initialiser, faites que ça soit une liste modifiable)
4. Exposer la liste au template dans le Model et faire en sorte de boucler dessus dans le todo.html
5. Rajouter un formulaire dans le template qui aura pour le moment juste un input
6. Créer une route pour le POST dans le contrôleur et faire qu'on récupère la valeur de l'input (avec le ModelAttribute) et qu'on fasse un add sur la liste pour rajouter un item à notre liste 

## TodoList Version Object [Branche with-object](https://gitlab.com/simplonlyon/promo16/exo-spring/-/tree/with-object)
1. Créer une entité entity.PostIt qui aura 2 propriétés, un title en String et un content en String aussi, avec les getter/setter
2. Modifier le TodoController pour faire qu'au lieu d'avoir une List<String> on ait une List<PostIt>
3. Modifier l'affichage du template pour faire qu'au lieu d'afficher juste l'item, on affiche le title de l'item et la description de l'item
4. Modifier le formulaire pour y ajouter un nouveau champ content, et modifier le PostMapping pour faire qu'on y crée une instance de PostIt qu'on ajoute à la liste
5. Bootstrappez moi tout ça en faisant un affichage sous forme de card dans des col
Bonus : Faire que le formulaire utilise directement un objet PostIt (il faudra utiliser un th:object sur le form et des th:field sur les inputs) 