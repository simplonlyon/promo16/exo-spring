package co.simplon.promo16.exosrping.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {
    
    @GetMapping("/")
    public String showHomePage(Model model) {
        String message = "From Controller";
        model.addAttribute("message", message);

        Integer randomNumber = new Random().nextInt(10);
        model.addAttribute("randomNumber", randomNumber);

        model.addAttribute("currentTime", LocalDateTime.now());

        return "homepage";
    }

    @GetMapping("/loop")
    public String showList(Model model) {
        List<String> list = List.of("ga", "zo", "bu", "meu");
        model.addAttribute("list", list);
        return "loop";
    }

    @GetMapping("/form")
    public String showForm(Model model) {
        System.out.println("from GET");
        return "form";
    }

    @PostMapping("/form")
    public String processForm(@ModelAttribute("message") String message) {
        System.out.println("from POST");
        System.out.println(message);
        return "form";
    }

    @GetMapping("/{name}")
    public String parametrizedRoute(@PathVariable("name") String name) {
        System.out.println(name);
        return "parametrized";
    }
}

