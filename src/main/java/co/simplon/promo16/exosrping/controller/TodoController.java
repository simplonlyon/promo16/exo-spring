package co.simplon.promo16.exosrping.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TodoController {
    private List<String> list = new ArrayList<>(List.of("test", "toust", "autre"));

    @GetMapping("/todo")
    public String showTodos(Model model) {
        model.addAttribute("list", list);
        return "todo";
    }

    @PostMapping("/todo")
    public String addTodo(@ModelAttribute("title") String title) {
        list.add(title);
        return "redirect:/todo";
    }

}
